$(document).ready(function() {
	"use strict";
	/*
	  ==============================================================
		   Home Page Main Slider
	  ==============================================================
	  */
	  if ($('.flexslider').length) {
	  	$('.flexslider').flexslider({
	  		animation: "slide",
	  		animationSpeed: 2000,
	  		animationLoop: true,
	  		pauseOnHover: true,
	  	});
	  };
	  $(".flex-direction-nav .flex-prev, .flex-direction-nav .flex-next").text("");
	/*
	  ==============================================================
		  Date Picker
	  ==============================================================
	  */
	  $('[data-toggle="datepicker"]').datepicker();
	/*
	  ==============================================================
		   Testimonial Slider
	  ==============================================================
	  */
	  if ($('.tm-testimonials-home .owl-carousel').length) {
	  	$(".tm-testimonials-home .owl-carousel").owlCarousel({
	  		loop: true,
	  		margin: 20,
			// nav:true,
			dots: true,
			responsiveClass: true,
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				767: {
					items: 3
				}
			}
		});
	  };
	/*
	  ==============================================================
		   Services Slider
	  ==============================================================
	  */
	  if ($('.tm-services-style-3 .owl-carousel').length) {
	  	$(".tm-services-style-3 .owl-carousel").owlCarousel({
	  		loop: true,
	  		margin: 20,
			// nav:true,
			// items:4,
			dots: true,
			responsiveClass: true,
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 2
				},
				768: {
					items: 4
				}
			}
		});
	  };
	/*
	  ==============================================================
		   Awards Slider
	  ==============================================================
	  */
	  if ($('.tm-awards .owl-carousel').length) {
	  	$(".tm-awards .owl-carousel").owlCarousel({
	  		loop: true,
	  		margin: 30,
			// items:6
			responsiveClass: true,
			responsive: {
				0: {
					items: 2
				},
				480: {
					items: 4
				},
				768: {
					items: 6
				}
			}
		});
	  };
	/*
	  ==============================================================
		   Filter Gallery
	  ==============================================================
	  */
	  if ($('.filtr-container').length) {
	  	$('.filtr-container').imagesLoaded( function() {

	  		var filterizd = $('.filtr-container').filterizr({
			//options object
		});
	  		$(".tm-filter-gallery .filter-tabs li:first-child").addClass("active");
	  		$(".tm-filter-gallery .filter-tabs li").on("click", function() {
	  			$(this).addClass("active");
	  			$(this).siblings("li").removeClass("active");
	  		});
	  	});
	  };
	/*
	  =======================================================================
		  		Map Script Script
	  =======================================================================
	  */
	  if ($('#map-canvas').length) {
	  	google.maps.event.addDomListener(window, 'load', initialize);
	  }
	  /* ---------------------------------------------------------------------- */
	/*	Google Map Function for Custom Style
	/* ---------------------------------------------------------------------- */
	function initialize() {
		var MY_MAPTYPE_ID = 'custom_style';
		var map;
		var brooklyn = new google.maps.LatLng(40.6743890, -73.9455);
		var featureOpts = [{
			stylers: [{
				hue: '#f9f9f9'
			}, {
				visibility: 'simplified'
			}, {
				gamma: 0.7
			}, {
				saturation: -200
			}, {
				lightness: 45
			}, {
				weight: 0.6
			}]
		}, {
			featureType: "road",
			elementType: "geometry",
			stylers: [{
				lightness: 200
			}, {
				visibility: "simplified"
			}]
		}, {
			elementType: 'labels',
			stylers: [{
				visibility: 'on'
			}]
		}, {
			featureType: 'water',
			stylers: [{
				color: '#ffffff'
			}]
		}];
		var mapOptions = {
			zoom: 15,
			scrollwheel: false,
			center: brooklyn,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
			},
			mapTypeId: MY_MAPTYPE_ID
		};
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		var styledMapOptions = {
			name: 'Custom Style'
		};
		var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
		map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
	};
	/*
	  =======================================================================
		  		General
	  =======================================================================
	  */
	  $(".search-btn .icn").on("click", function() {
	  	$(this).siblings('input[type="submit"]').on("click");
	  });
	  $(".topbar .t-options .icons_t span.fa").on("click", function() {
	  	$(".search-box").toggleClass("active");
	  	$(this).toggleClass("fa-times fa-search");
	  });
	  $(".topbar .t-options .icons_t").on("mouseover", function() {
	  	$(this).children(".cart-drop").addClass("active");
	  })
	  $(".topbar .t-options .icons_t").on("mouseout", function() {
	  	$(this).children(".cart-drop").removeClass("active");
	  });
	/*
	  =======================================================================
		  		Dl Mobile Menu
	  =======================================================================
	  */
	  $(function() {
	  	$('#dl-menu').dlmenu({
	  		animationClasses: {
	  			classin: 'dl-animate-in-5',
	  			classout: 'dl-animate-out-5'
	  		}
	  	});
	  });
	/*
	  =======================================================================
		  		Project Facts Counter
	  =======================================================================
	  */
	// Create your event handlers
	function enterView(e) {
		$('.count_num').each(function() {
			$(this).prop('Counter', 0).animate({
				Counter: $(this).text()
			}, {
				duration: 2000,
				easing: 'swing',
				step: function(now) {
					$(this).text(Math.ceil(now));
				}
			});
		});
	}

	function leaveView(e) {
		$('.count_num').text();
	}
	// Inside DOM-Loaded event
	$('.tm-project-facts')
		// Bind events to your handlers
		.on('enterviewport', enterView).on('leaveviewport', leaveView)
		// Initialize the plug-in
		.bullseye();
	/*
	  =======================================================================
  		Back to top
	  =======================================================================
	  */
	  $(window).scroll(function() {
	  	if ($(this).scrollTop()) {
	  		$('#backTop').fadeIn();
	  	} else {
	  		$('#backTop').fadeOut();
	  	}
	  });
	  $("#backTop").on('click', function() {
		//1 second of animation time
		//html works for FFX but not Chrome
		//body works for Chrome but not FFX
		//This strange selector seems to work universally
		$("html, body").animate({
			scrollTop: 0
		}, 1000);
	});
	});